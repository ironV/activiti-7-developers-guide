# activiti7 官方开发者文档的镜像

## 官方文档部署在github上，打开慢，我克隆到本地 然后打包成pdf 可以直接看

# 1.下载pdf，推荐

[book.pdf下载](book.pdf)

[百度云下载](https://pan.baidu.com/s/1FlP3aNRjDChbLWpfxyokHA)  提取码: 68xk


# 2.自行编译运行


```
npm install -g gitbook-cli
gitbook -V

gitbook install

gitbook server
```

在执行过程中出现`TypeError: cb.apply is not a function`错误，这时需打开

`C:\Users\User\AppData\Roaming\npm\node_modules\gitbook-cli\node_modules\npm\node_modules\graceful-fs\polyfills.js`

在第62-64行调用了这个函数
```
fs.stat = statFix(fs.stat)
fs.fstat = statFix(fs.fstat)
fs.lstat = statFix(fs.lstat)
```
把这三行代码注释掉就解决报错了

# 3.自行编译pdf

在上一步的基础上，运行`gitbook pdf`


版权 https://github.com/Activiti  修改了部分内容，让其方便编译通过